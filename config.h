#define URL ""
#define PORT 
#define USER ""
#define PWD ""
#define VERSION ""
#define APP ""
#define USE_CACHE 1
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

enum { selected, highlighted, inactive };
static const int colors[][2] = {
    /*               fg, bg */
    [selected] =    {7,  3},
    [highlighted] = {5,  7},
    [inactive] =    {3,  0},
};

const char *playing_indicator = "> ";
const char *slider_unplayed = "-";
const char *slider_played = "#";
