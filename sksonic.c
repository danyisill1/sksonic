#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <curl/curl.h>
#include "cJSON.c"
#include <time.h>
#include <locale.h>
#include "config.h"

struct url_data {
    size_t size;
    char *data;
};

struct conn {
    char *url;
    int  port;
    char *user;
    char *password;
    char *version;
    char *app;
};

struct song {
    char *id;
    char *artist;
    char *album;
    char *title;
    int  duration;
};

struct information {
    char **id;
    char **name;
    int  *duration;
    int  len;
    int  sel;
};

struct playlist {
    struct song *songs;
    int     current_playing;
    int     duration;
    int     total_played;
    int     playing;            /*Values 0: stopped, 1: playing, 2: paused */
    time_t  time;
    int     sel;
    int     len;
};

struct cached_song {
    char *artist_id;
    char *artist;
    char *album_id;
    char *album;
    char *song_id;
    char *song_title;
    int  song_duration;
};

struct cache {
    struct cached_song *songs;
    int len;
};

size_t write_data(void *ptr, size_t size, size_t nmemb, struct url_data *data);
int test_connection(struct conn *conn);
int album_in_cache(char *album_id, struct cache *cache);
void print_error(char *error);
void get_artists(char *url, struct information *artists);
void get_albums(char *artist_id, struct information *info, struct conn *conn);
void get_songs(char *album_id, struct information *info, struct conn *conn, struct cache *cache);
void update_cache(struct information **info, struct cache *cache, int artist_pos, int album_pos);
void trim_text(char *original_text, char *padding, size_t max_length, char *destination_txt);
void print_window(WINDOW **windows, int n_windows, int active, struct information **songs_array, struct playlist **pl_array, const char **appearance);
void print_playing_song(WINDOW *window, struct playlist *p, const char **appearance);
void print_playlist(WINDOW *window, struct playlist *pl, int current_line);
void add_song(struct information **info, int artist_pos, int album_pos, int song_pos, struct playlist *pl);
void delete_song(char *song_id, struct playlist *pl, struct playlist **pl_array);
void play_song(char *song_id, FILE *fp, struct playlist *pl, struct conn *conn);
void control_player(char *command, struct playlist *pl, struct conn *conn);
char *sonic_url(struct conn *conn, char *operation, char *data);
char *get_data(char *url);
char *prepend_text(char *original_text, const char *prepend);

size_t write_data(void *ptr, size_t size, size_t nmemb, struct url_data *data)
{
    size_t index = data->size;
    size_t n = (size * nmemb);
    char *tmp;

    data->size += (size * nmemb);
    tmp = realloc(data->data, data->size + 1); /* +1 for '\0' */

    if(tmp)
    {
        data->data = tmp;
    } 
    else 
    {
        if(data->data)
        {
            free(data->data);
        }
        fprintf(stderr, "Failed to allocate memory.\n");
        return 0;
    }

    memcpy((data->data + index), ptr, n);
    data->data[data->size] = '\0';
    return size * nmemb;
}

int test_connection(struct conn *conn)
{
    CURL *curl;
    CURLcode res;
    int subsonic_ok;
    char *url;
    struct url_data data;
    data.size = 0;
    data.data = malloc(4096);

    if(NULL == data.data)
    {
        fprintf(stderr, "Failed to allocated memory.\n");
        exit(1);
    }
    data.data[0] = '\0';

    
    curl = curl_easy_init();
    if(curl)
    {
        url = sonic_url(conn, "ping", NULL);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long)CURL_HTTP_VERSION_2);
        free(url);

        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
        curl_easy_cleanup(curl);

        cJSON *root = cJSON_Parse(data.data);
        cJSON *subsonic = cJSON_GetObjectItem(root, "subsonic-response");
        free(data.data);
        subsonic_ok = strcmp("ok", cJSON_GetObjectItem(subsonic, "status")->valuestring) == 0 ? 1 : 0;
        cJSON_Delete(root);
        if (subsonic_ok)
        {
            return 1;
        }
        else
        {
            fprintf(stderr, "Cannot connect to the subsonic server.\n");
            exit(1);
        }
    }
    return 2;
}

void print_error(char *error)
{
    fprintf(stderr, "%s", error);
    exit(1);
}

char *sonic_url(struct conn *conn, char *operation, char *data)
{
    char *url;
    int len_url;

    if (strcmp(operation, "ping") == 0)
    {
        len_url = snprintf(NULL, 0, "%s:%d/rest/ping.view?u=%s&p=%s&v=%s&c=%s&f=json", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app);
        url = malloc(sizeof(char)*(len_url+1));
        if (NULL == url) 
        {
            print_error("Failed to allocated memory for the url.\n"); 
        }
        sprintf(url, "%s:%d/rest/ping.view?u=%s&p=%s&v=%s&c=%s&f=json", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app);
    }
    if (strcmp(operation, "artists") == 0)
    {
        len_url = snprintf(NULL, 0, "%s:%d/rest/getArtists?f=json&u=%s&p=%s&v=%s&c=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app);
        url = malloc(sizeof(char)*(len_url+1));
        if (NULL == url) 
        { 
            print_error("Failed to allocated memory for the url.\n"); 
        }
        sprintf(url, "%s:%d/rest/getArtists?f=json&u=%s&p=%s&v=%s&c=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app);
    }
    if (strcmp(operation, "albums") == 0)
    {
        len_url = snprintf(NULL, 0, "%s:%d/rest/getArtists?f=json&u=%s&p=%s&v=%s&c=%s&id=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app, data);
        url = malloc(sizeof(char)*(len_url+1));
        if (NULL == url) 
        { 
            print_error("Failed to allocated memory for the url.\n"); 
        }
        sprintf(url, "%s:%d/rest/getArtist?f=json&u=%s&p=%s&v=%s&c=%s&id=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app, data);
    }
    if (strcmp(operation, "songs") == 0)
    {
        len_url = snprintf(NULL, 0, "%s:%d/rest/getAlbum?f=json&u=%s&p=%s&v=%s&c=%s&id=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app, data);
        url = malloc(sizeof(char)*(len_url+1));
        if (NULL == url) 
        {
            print_error("Failed to allocated memory for the url.\n"); 
        }
        sprintf(url, "%s:%d/rest/getAlbum?f=json&u=%s&p=%s&v=%s&c=%s&id=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app, data);
    }
    if (strcmp(operation, "play") == 0)
    {
        len_url = snprintf(NULL, 0, "%s:%d/rest/stream?u=%s&p=%s&v=%s&c=%s&id=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app, data);
        url = malloc(sizeof(char)*(len_url+1));
        if (NULL == url) 
        { 
            print_error("Failed to allocated memory for the url.\n");
        }
        sprintf(url, "%s:%d/rest/stream?u=%s&p=%s&v=%s&c=%s&id=%s", conn->url, conn->port, conn->user, conn->password, conn->version, conn->app, data);
    }
    return url;
}

char *get_data(char *url)
{
    CURL *curl;
    struct url_data data;
    data.size = 0;
    data.data = malloc(4096);
    if(NULL == data.data) { print_error("Failed to allocate memory.\n"); }
    data.data[0] = '\0';

    CURLcode res;
    
    curl = curl_easy_init();
    if(curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long)CURL_HTTP_VERSION_2);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
        }
        curl_easy_cleanup(curl);
    }
    return data.data;
}

void free_struct_info(struct information *info)
{
    int i;
    if (info->id != NULL)
    {
        for (i=0; i<info->len; i++)
        {
            free(info->id[i]);
            info->id[i] = NULL;
        }
    free(info->id);
    info->id = NULL;
    }
    if (info->name != NULL)
    {
        for (i=0; i<info->len; i++)
        {
            free(info->name[i]);
            info->name[i] = NULL;
        }
    free(info->name);
    info->name = NULL;
    }
    if (info->duration != NULL)
    {
    free(info->duration);
    info->duration = NULL;
    }
}

void free_struct_pl(struct playlist *pl)
{
    if (pl->songs != NULL)
    {
        free(pl->songs);
        pl->songs = NULL;
    }
    return;
}



void get_artists(char *url, struct information *info)
{
    char *data;

    data = get_data(url);

    free_struct_info(info);
    info->len = 0;

    cJSON *root = cJSON_Parse(data);
    cJSON *s = cJSON_GetObjectItem(root, "subsonic-response");
    cJSON *sa = cJSON_GetObjectItem(s, "artists");
    cJSON *index = cJSON_GetObjectItem(sa, "index");
    cJSON *initial;
    cJSON_ArrayForEach(initial, index)
    {
        cJSON *artists_initial = cJSON_GetObjectItem(initial, "artist");
        cJSON *artist;
        cJSON_ArrayForEach(artist, artists_initial)
        {
            info->id = realloc(info->id, sizeof(char*)*(info->len + 1));
            info->name = realloc(info->name, sizeof(char*)*(info->len + 1));
            info->id[info->len] = malloc(sizeof(char)*(strlen(cJSON_GetObjectItem(artist, "id")->valuestring)+1));
            strcpy(info->id[info->len], cJSON_GetObjectItem(artist, "id")->valuestring);

            info->name[info->len] = malloc(sizeof(char)*(strlen(cJSON_GetObjectItem(artist, "name")->valuestring)+1));
            strcpy(info->name[info->len], cJSON_GetObjectItem(artist, "name")->valuestring);
            info->len += 1;
        }
    }
    cJSON_Delete(root);
    free(data);
    return;
}

void get_albums(char *artist_id, struct information *info, struct conn *conn)
{
    char *data;
    char *url;

    url = sonic_url(conn, "albums", artist_id);
    data = get_data(url);

    free_struct_info(info);
    info->len = 0;

    cJSON *root = cJSON_Parse(data);
    cJSON *s = cJSON_GetObjectItem(root, "subsonic-response");
    cJSON *sa = cJSON_GetObjectItem(s, "artist");
    cJSON *al = cJSON_GetObjectItem(sa, "album");
    cJSON *album_item;
    cJSON_ArrayForEach(album_item, al)
    {
        info->id = realloc(info->id, sizeof(char*)*(info->len + 1));
        info->name = realloc(info->name, sizeof(char*)*(info->len + 1));
        info->id[info->len] = malloc(sizeof(char)*(strlen(cJSON_GetObjectItem(album_item, "id")->valuestring)+1));
        strcpy(info->id[info->len], cJSON_GetObjectItem(album_item, "id")->valuestring);
        info->name[info->len] = malloc(sizeof(char)*(strlen(cJSON_GetObjectItem(album_item, "name")->valuestring)+1));
        strcpy(info->name[info->len], cJSON_GetObjectItem(album_item, "name")->valuestring);
        info->len += 1;
    }
    cJSON_Delete(root);
    free(data);
    free(url);
    return;
}

int album_in_cache(char *album_id, struct cache *cache)
{
    int i;
    for (i = 0; i < cache->len; i++)
    {
        if (cache->songs[i].album_id == album_id)
        {
            return 1;
        }
    }
    return 0;
}

void get_songs(char *album_id, struct information *info, struct conn *conn, struct cache *cache)
{
    int i;

    info->len = 0;

    i = 0;
    if (album_in_cache(album_id, cache) == 1)
    {
        for (i=0; i < cache->len; i++)
        {
            if (cache->songs[i].album_id == album_id)
            {
                info->id = realloc(info->id, sizeof(char*)*(info->len + 1));
                info->name = realloc(info->name, sizeof(char*)*(info->len + 1));
                info->duration = realloc(info->duration, sizeof(int)*(info->len + 1));

                info->name[info->len] = cache->songs[i].song_title;
                info->id[info->len] = cache->songs[i].song_id;
                info->duration[info->len] = cache->songs[i].song_duration;
                info->len += 1;
            }
        }
    }
    else
    {
        char *data;
        char *url;

        url = sonic_url(conn, "songs", album_id);
        data = get_data(url);

        cJSON *root = cJSON_Parse(data);
        cJSON *s = cJSON_GetObjectItem(root, "subsonic-response");
        cJSON *al = cJSON_GetObjectItem(s, "album");
        cJSON *ss = cJSON_GetObjectItem(al, "song");
        cJSON *song_item;
        cJSON_ArrayForEach(song_item, ss)
        {
            info->id = realloc(info->id, sizeof(char*)*(info->len + 1));
            info->name = realloc(info->name, sizeof(char*)*(info->len + 1));
            info->duration = realloc(info->duration, sizeof(int)*(info->len + 1));

            info->name[info->len] = malloc(sizeof(char)*(strlen(cJSON_GetObjectItem(song_item, "title")->valuestring)+1));
            strcpy(info->name[info->len] , cJSON_GetObjectItem(song_item, "title")->valuestring);
            info->id[info->len] = malloc(sizeof(char)*(strlen(cJSON_GetObjectItem(song_item, "id")->valuestring)+1));
            strcpy(info->id[info->len], cJSON_GetObjectItem(song_item, "id")->valuestring);
            if (cJSON_HasObjectItem(song_item, "duration"))
                info->duration[info->len] = cJSON_GetObjectItem(song_item, "duration")->valueint;
            else
                info->duration[info->len] = 1;
            info->len += 1;
        }
        cJSON_Delete(root);
        free(data);
        free(url);
    }
    return;
}

void update_cache(struct information **info, struct cache *cache, int artist_pos, int album_pos)
{

    int i, j, cached;

    cached = 0;
    i = 0;
    while(cached == 0 && i < cache->len)
    {
        if (cache->songs[i].album_id == info[1]->id[album_pos])
        {
            cached = 1;
            break;
        }
        i += 1;
    }

    if (cached == 1)
    {
        return;
    }
    else /* Update the cache */
    {
        cache->songs = realloc(cache->songs, sizeof(struct cached_song)*(cache->len + info[2]->len));
        for (i=0; i<info[2]->len; i++)
        {
            j = cache->len + i;
            cache->songs[j].artist_id = info[0]->id[artist_pos];
            cache->songs[j].artist = info[0]->name[artist_pos];
            cache->songs[j].album_id = info[1]->id[album_pos];
            cache->songs[j].album = info[1]->name[album_pos];
            cache->songs[j].song_id = info[2]->id[i];
            cache->songs[j].song_title = info[2]->name[i];
            cache->songs[j].song_duration = info[2]->duration[i];
        }
        cache->len += info[2]->len;
    }
}

void trim_text(char *original_text, char *padding, size_t max_length, char *return_text)
{
    size_t i;
    char *txt;

    txt = malloc(sizeof(char)*(max_length+1));
    if (NULL == txt) { print_error("Failed to allocate memory.\n"); }

    if (strlen(original_text) > max_length)
    {
        snprintf(txt, max_length-strlen(padding)-2, "%s", original_text);
        strcat(txt, padding);
    }
    else
    {
        sprintf(txt, original_text, strlen(original_text)-1);
        for (i = 0; i < max_length - strlen(original_text); i++)
        {
            strcat(txt, " ");
        }
    }
    strcpy(return_text, txt);
    free(txt);
}

char *prepend_text(char *original_text, const char *prepend)
{
    char *txt;
    txt = malloc(strlen(original_text) + strlen(prepend) + 1);
    if (NULL == txt) { print_error("Failed to allocate memory.\n"); }

    sprintf(txt, "%s%s", prepend, original_text);
    return txt;
}

void setup_ncurses()
{
    enum { selected, highlighted, inactive };
    enum { fg, bg };
    setlocale(LC_ALL, "");
    initscr();
    clear();
    noecho();
    curs_set(0);
    cbreak();
    halfdelay(1);
    keypad(stdscr, TRUE);
    start_color();
    init_pair(1, colors[selected][fg],    colors[selected][bg]);
    init_pair(2, colors[highlighted][fg], colors[highlighted][bg]);
    init_pair(3, colors[inactive][fg],    colors[inactive][bg]);
    return;
}

int erase_playlist(WINDOW **windows, int n_windows, char *message)
{
    char c;
    mvwprintw(windows[n_windows], 1, 1, "%s", message);
    wrefresh(windows[n_windows]);
    cbreak();
    c = getch();
    halfdelay(1);
    if (c == 'y' || c == 'Y')
    {
        return 1;
    }
    else
    {
        return 0;
    }
    return 0;
}

void print_window(WINDOW **windows, int n_windows, int active, struct information **songs_array, struct playlist **pl_array, const char **appearance)
{
    int i, k;
    int array_sel, array_len, first, last, offset;
    int maxy, maxx;
    int txt_len;
    int max_size, max_len;
    char *txt;
    enum { selected = 1, highlighted, inactive };

    if (songs_array == NULL)
    {
        array_len = pl_array[0]->len;
        array_sel = pl_array[0]->sel;
    }

    for (k = 0; k < n_windows; k++)
    {
        if (songs_array != NULL)
        {
            array_len = songs_array[k]->len;
            array_sel = songs_array[k]->sel;
        }
        getmaxyx(windows[k], maxy, maxx);
        max_size = maxy-2;
        max_len = maxx-2;
        werase(windows[k]);
        box(windows[k], ACS_VLINE, ACS_HLINE);

        if (array_len <= max_size)
        {
            offset = 0;
            first = 0;
            last = array_len;
        }
        else
        {
            if (array_sel <= max_size/3*2)
            {
                offset = 0;
                first = 0;
                last = max_size;
            }
            else
            {
                first = MAX(0, MIN(array_sel - max_size/3*2, array_len - max_size));
                last = MIN(first + max_size, array_len);
                offset = first;
                }
        }

        for(i = first; i < last; i++)
        {
            txt = malloc(sizeof(char)*(max_len+1));
            if (songs_array == NULL)
            {
                txt_len = strlen(pl_array[k]->songs[i].artist) + strlen(pl_array[k]->songs[i].album) + strlen(pl_array[k]->songs[i].title) + 3;
                txt = malloc(sizeof(char*)*txt_len);
                sprintf(txt, "%s", pl_array[k]->songs[i].artist);
                strcat(txt, " - ");
                strcat(txt, pl_array[k]->songs[i].album);
                strcat(txt, " - ");
                strcat(txt, pl_array[k]->songs[i].title);
                if (pl_array[k]->playing > 0 && pl_array[k]->current_playing == i - offset)
                {
                    txt = realloc(txt, sizeof(char)*(max_len-strlen(appearance[0])+1));
                    trim_text(txt, ".", max_len-strlen(appearance[0]), txt);
                    txt = prepend_text(txt, appearance[0]);
                }
                else
                {
                    txt = realloc(txt, sizeof(char)*(max_len+1));
                    trim_text(txt, ".", max_len, txt);
                }
            }
            else
            {
                trim_text(songs_array[k]->name[i], "...", max_len, txt);
            }

            if(i  == array_sel - 1)
            {
                if (k == active)
                {
                    wattron(windows[k], COLOR_PAIR(selected));
                }
                else {
                    wattron(windows[k], COLOR_PAIR(highlighted));
                }
                mvwprintw(windows[k], i-offset+1, 1, "%s", txt);
                wattron(windows[k], COLOR_PAIR(inactive));
            }
            else 
            {
                mvwprintw(windows[k], i-offset+1, 1, "%s", txt);
            }
            free(txt);
        }
        wrefresh(windows[k]);
    }
    if (songs_array != NULL)
    {
        print_playing_song(windows[n_windows], pl_array[0], appearance);
    }
}

void print_playing_song(WINDOW *window, struct playlist *pl, const char **appearance)
{
    char *status;
    int maxy, maxx;
    int i;
    int already_played;

    getmaxyx(window, maxy, maxx);

    if (pl->playing == 0)
    {
        werase(window);
        wrefresh(window);
        return;
    }
    else
    {
        werase(window);
        already_played = (int) ((float) pl->total_played/(float) pl->songs[pl->current_playing].duration*maxx);
        for (i=0; i<maxx-1; i++)
        {
            if (i <= already_played)
            {
                mvwprintw(window, 2, i+1, "%s", appearance[2]);
            }
            else
            {
                mvwprintw(window, 2, i+1, "%s", appearance[1]);
            }

        }

        status = (pl->playing == 1) ? "Playing" : "Paused";
        mvwprintw(window, 1, 1, "%s - %s - %s [%s]", pl->songs[pl->current_playing].artist, pl->songs[pl->current_playing].album, pl->songs[pl->current_playing].title, status);
        wrefresh(window);
    }
}
void print_playlist(WINDOW *window, struct playlist *pl, int current_line)
{
    int i;
    int txt_len;
    int first, last, offset;
    int maxy, maxx;

    getmaxyx(window, maxy, maxx);
    int max_size = maxy-2;
    int max_len = maxx-2;
    char *txt;
    char *prepend_txt = "> ";
    werase(window);

    if (pl->len < max_size)
    {
        offset = 0;
        first = 0;
        last = pl->len;
    }
    else
    {
        if (current_line <= max_size/3*2)
        {
            offset = 0;
            first = 0;
            last = max_size;
        }
        else
        {
            first = MAX(0, MIN(current_line - max_size/3*2, pl->len - max_size));
            last = MIN(first + max_size, pl->len);
            offset = first;
        }
    }


    for(i = first; i < last; i++)
    {
        txt_len = strlen(pl->songs[i].artist) + strlen(pl->songs[i].album) + strlen(pl->songs[i].title) + 3;
        txt = malloc(sizeof(char*)*txt_len);
        sprintf(txt, "%s", pl->songs[i].artist);
        strcat(txt, " - ");
        strcat(txt, pl->songs[i].album);
        strcat(txt, " - ");
        strcat(txt, pl->songs[i].title);

        if (pl->playing > 0 && pl->current_playing == i - offset)
        {
            txt = realloc(txt, sizeof(char)*(max_len-strlen(prepend_txt)+1));
            trim_text(txt, ".", max_len-strlen(prepend_txt), txt);
            txt = prepend_text(txt, prepend_txt);
        }
        else
        {
            txt = realloc(txt, sizeof(char)*(max_len+1));
            trim_text(txt, ".", max_len, txt);
        }
        if(i  == current_line - 1)
        {
            wattron(window, COLOR_PAIR(3));
            mvwprintw(window, i-offset+1, 1, "%s", txt);
            wattron(window, COLOR_PAIR(1));
        }
        else 
        {
            mvwprintw(window, i-offset+1, 1, "%s", txt);
        }
        free(txt);
    }
    wrefresh(window);
}

void add_song(struct information **info, int artist_pos, int album_pos, int song_pos, struct playlist *pl)
{
    int i;
    int j;
    for (i=0; i < pl->len; i++)
    {
        if (pl->songs[i].id == info[2]->id[song_pos])
        {
            return;
        }
    }

    j = pl->len;
    pl->songs = realloc(pl->songs, (j+1)*sizeof(struct song));
    pl->songs[j].artist = info[0]->name[artist_pos];
    pl->songs[j].album = info[1]->name[album_pos];
    pl->songs[j].title = info[2]->name[song_pos];
    pl->songs[j].id = info[2]->id[song_pos];
    pl->songs[j].duration = info[2]->duration[song_pos];
    pl->len += 1;
}
        
void delete_song(char *song_id, struct playlist *pl, struct playlist **pl_array)
{
    int i;
    int j=0;
    struct song *tmp;

    if (pl->len == 0)
    {
        return;
    }
    else
    {
        tmp = malloc((pl->len-1)*sizeof(struct song));
        for (i=0; i< pl->len; i++)
        {
            if (pl->songs[i].id == song_id)
            {
                continue;
            }
            //tmp[j].title = pl->songs[i].title;
            //tmp[j].id = pl->songs[i].id;
            //tmp[j].duration = pl->songs[i].duration;
            tmp[j] = pl->songs[i];
            j++;
        }
        pl->songs = realloc(pl->songs, (pl->len-1)*sizeof(struct song));
        for (i=0; i < pl->len-1; i++)
        {
            pl->songs[i] = tmp[i];
        }
        pl->len -= 1;
    }
    pl_array[0] = pl;
}

void play_song(char *song_id, FILE *fp, struct playlist *pl, struct conn *conn)
{
    char command[2048];
    char *url;
    int i;

    /* If something was already being played, kill ffplay */
    if (pl->playing > 0)
    {
        control_player("stop", pl, conn);
    }

    /* Play the song song_id */
    url = sonic_url(conn, "play", song_id);
    sprintf(command, "ffplay -nodisp -autoexit \"%s\" 2>&1", url);
    free(url);
    fp = popen(command, "r");
    pl->playing = 1;
    pl->time = time(NULL);

    /* Update the value of current_playing to reflect the position in pl->songs array */
    for (i=0; i < pl->len; i++)
    {
        if (pl->songs[i].id == song_id)
        {
            pl->current_playing = i;
        }
    }
}

void control_player(char *command, struct playlist *pl, struct conn *conn)
{
    FILE *fp = NULL;
    char process[128];
    char kill[256];
    fp = popen("ps aux | grep ffplay | grep -v sh | tr -s \" \" | cut -d \" \" -f 2", "r");
    fgets(process, 128, fp);
    fclose(fp);
    if (strcmp(command, "stop") == 0)
    {
        sprintf(kill, "kill -9 %s", process);
        fp = popen(kill, "r");
        fclose(fp);
        pl->playing = 0;
        pl->current_playing = -99;
        pl->total_played = 0;
    }
    if (strcmp(command, "toggle") == 0)
    {
        switch (pl->playing)
        {
            case 1:
                sprintf(kill, "kill -19 %s", process);
                pl->playing = 2;
                break;
            case 2:
                sprintf(kill, "kill -18 %s", process);
                pl->playing = 1;
                break;
        }
        fp = popen(kill, "r");
        fclose(fp);
    }
    if (strcmp(command, "next") == 0 || strcmp(command, "prev") == 0)
    {
        if (strcmp(command, "next") == 0 && pl->current_playing < pl->len)
        {
            pl->current_playing += 1;
        }
        if (strcmp(command, "prev") == 0 && pl->current_playing > 0)
        {
            pl->current_playing -= 1;
        }
        if (pl->playing == 1)
        {
            control_player("kill", NULL, NULL);
        }
        play_song(pl->songs[pl->current_playing].id, fp, pl, conn);
    }
}

void create_windows(WINDOW *screen, WINDOW **windows, int n_horizontal, int n_vertical, int size_vertical)
{
    /* Returns n_horizontal equally spaced windows, and n_vertical windows of size size_vertical */
    /* Example with n_horizontal = 3, n_vertical = 2, size_vertical = 1
     * ----------+----------+----------
     * |         |          |         |
     * |         |          |         |
     * |         |          |         |
     * +---------+----------+---------+
     * |         |          |         |
     * +---------+----------+---------+
     * |         |          |         |
     * +---------+----------+---------+
     */

    int x, y, i, x_size, x_acc;
    getmaxyx(screen, y, x);

    x_acc = 0;
    for (i = 0; i < n_horizontal; i++)
    {
        x_size = (x-x_acc)/(n_horizontal-i);
        windows[i] = newwin(y-n_vertical*size_vertical, x_size, 0, x_acc);
        x_acc += x_size;
    }
    for (i = 0; i < n_vertical; i++)
    {
        windows[n_horizontal+i] = newwin(size_vertical, 0, y - (i+1)*size_vertical, 0);
    }
}

int main() {
    struct conn conn = {URL, PORT, USER, PWD, VERSION, APP};
    test_connection(&conn); /* Exits upon failure */

    int c;
    int current_window = 0;
    int current_panel = 1;
    FILE *fp = NULL;
    int h_windows = 3;
    int v_windows = 1;
    int p_windows = 1;


    /* Iterators to add all the albums for an artist or all the songs of an album to the playlist */
    int iter_albums;
    int iter_songs;

    /* The playlist object */
    struct playlist pl;
    pl.songs = NULL;
    pl.playing = 0;
    pl.total_played = 0;
    pl.len = 0;
    pl.sel = 1;

    /* Set up the cache if selected */
    struct cache cache;
    cache.songs = NULL;
    cache.len = 0;

    struct information artists;
    artists.len = 0;
    artists.sel = 1;
    artists.id = NULL;
    artists.name = NULL;
    artists.duration = NULL;

    struct information albums;
    albums.len = 0;
    albums.sel = 1;
    albums.id = NULL;
    albums.name = NULL;
    albums.duration = NULL;

    struct information songs;
    songs.len = 0;
    songs.sel = 1;
    songs.id = NULL;
    songs.name = NULL;
    songs.duration = NULL;
    
    struct information *info[h_windows];
    info[0] = &artists;
    info[1] = &albums;
    info[2] = &songs;

    struct playlist *info_pl[p_windows];
    info_pl[0] = &pl;

    const char *appearance[3];
    appearance[0] = playing_indicator;
    appearance[1] = slider_unplayed;
    appearance[2] = slider_played;
    
    /* Populate with the initial data */
    get_artists(sonic_url(&conn, "artists", NULL), &artists);
    get_albums(artists.id[artists.sel-1], &albums, &conn);
    get_songs(albums.id[albums.sel-1], &songs, &conn, &cache);

    /* Start ncurses */
    setup_ncurses();

    WINDOW *windows[h_windows + v_windows];
    create_windows(stdscr, windows, h_windows, v_windows, 4);
    print_window(windows, h_windows, current_window, info, info_pl, appearance);

    WINDOW *windows_pl[2];
    create_windows(stdscr, windows_pl, 1, 1, 2);


    while(true)
    {
        c = getch();
        /* Window resize */
        if (c == KEY_RESIZE)
        {
            endwin();
            setup_ncurses();

            create_windows(stdscr, windows, h_windows, v_windows, 4);
            create_windows(stdscr, windows_pl, 1, 1, 2);

            print_window(windows, h_windows, current_window, info, info_pl,appearance);
        }
        /* Draw the information about the currently playing song */
        if (pl.playing == 1 && pl.total_played >= pl.songs[pl.current_playing].duration)
        {
            if (pl.current_playing == pl.len - 1)   // If playing the last song in the playlist
            {
                control_player("stop", &pl, &conn);  // Then stop the player
            }
            else                                            // Else
            {
                control_player("next", &pl, &conn);  // Play the next song in the playlist
            }
        }

        /* Update the playlist timings */
        if (pl.playing == 2) // Paused
        {
            pl.time = time(NULL);
        }
        if (pl.playing == 1) // Playing
        {
            pl.total_played = pl.total_played + time(NULL) - pl.time;
            pl.time = time(NULL);
        }

        /* Panel with artists, albums and songs */
        if (current_panel == 1)
        {

            /* Window with artists */
            if (current_window == 0)
            {
                print_window(windows, h_windows, current_window, info, info_pl, appearance);

                if (c == KEY_DOWN)
                {
                    if (artists.sel < artists.len)
                    {
                        artists.sel += 1;

                        get_albums(artists.id[artists.sel-1], &albums, &conn);
                        get_songs(albums.id[albums.sel-1], &songs, &conn, &cache);
                        if (USE_CACHE == 1)
                        {
                            update_cache(info, &cache, artists.sel-1, albums.sel-1);
                        }

                    }
                }
                if (c == KEY_UP)
                {
                    if (artists.sel > 1)
                    {
                        artists.sel -= 1;

                        get_albums(artists.id[artists.sel-1], &albums, &conn);
                        get_songs(albums.id[albums.sel-1], &songs, &conn, &cache);
                    }
                }
                if (c == KEY_RIGHT)
                {
                    current_window += 1;
                    albums.sel = 1;
                    songs.sel = 1;
                    continue;
                }
                if (c == 10 || c == KEY_ENTER) // Add the entire list of albums to the playlist
                {
                    for (iter_albums = 0; iter_albums < albums.len; iter_albums++)
                    {
                        get_songs(albums.id[iter_albums], &songs, &conn, &cache);
                        for (iter_songs = 0; iter_songs < songs.len; iter_songs++)
                        {
                            /* Immediately start playing the first song of the first album */
                            if (iter_songs == 0 && iter_albums == 0)
                            {
                                add_song(info, artists.sel-1, iter_albums, iter_songs, &pl);
                                play_song(songs.id[iter_songs], NULL, &pl, &conn);
                                if (USE_CACHE == 1)
                                {
                                    update_cache(info, &cache, artists.sel-1, iter_albums);
                                }
                            }
                            else
                            {
                                add_song(info, artists.sel-1, iter_albums, iter_songs, &pl);
                                if (USE_CACHE == 1)
                                {
                                    update_cache(info, &cache, artists.sel-1, iter_albums);
                                }
                            }
                        }
                    }
                    /* Restore the value of songs, so we can print it in the loop */
                    get_songs(albums.id[albums.sel-1], &songs, &conn, &cache);
                }
            }

            /* Window with albums */
            if (current_window == 1)
            {
                print_window(windows, h_windows, current_window, info, info_pl, appearance);
                if (c == KEY_DOWN)
                {
                    if (albums.sel < albums.len)
                    {
                        albums.sel += 1;

                        get_songs(albums.id[albums.sel-1], &songs, &conn, &cache);
                        if (USE_CACHE == 1)
                        {
                            update_cache(info, &cache, artists.sel-1, albums.sel-1);
                        }
                    }
                }
                if (c == KEY_UP)
                {
                    if (albums.sel > 1)
                    {
                        albums.sel -=1;
                        get_songs(albums.id[albums.sel-1], &songs, &conn, &cache);
                    }
                }
                if (c == KEY_LEFT)
                {
                    current_window -= 1;
                    albums.sel = 1;
                    songs.sel = 1;
                }
                if (c == KEY_RIGHT)
                {
                    current_window += 1;
                    songs.sel = 1;
                    continue;
                }
                if (c == 10 || c == KEY_ENTER) // Add the entire list of songs to the playlist
                {
                    for (iter_songs = 0; iter_songs < songs.len; iter_songs++)
                    {
                        /* Immediately start playing the first song of the first album */
                        if (iter_songs == 0)
                        {
                            add_song(info, artists.sel-1, albums.sel-1, iter_songs, &pl);
                            play_song(songs.id[iter_songs], NULL, &pl, &conn);
                        }
                        else
                        {
                            add_song(info, artists.sel-1, albums.sel-1, iter_songs, &pl);
                        }
                    }
                }
            }

            /* Window with songs */
            if (current_window == 2)
            {
                print_window(windows, h_windows, current_window, info, info_pl, appearance);
                if (c == KEY_DOWN)
                {
                    if (songs.sel < songs.len)
                    {
                        songs.sel += 1;
                    }
                }
                if (c == KEY_UP)
                {
                    if (songs.sel > 1)
                    {
                        songs.sel -= 1;
                    }
                }
                if (c == KEY_LEFT)
                {
                    current_window -= 1;
                    songs.sel = 1;
                }
                if (c == 10 || c == KEY_ENTER)
                {
                    add_song(info, artists.sel-1, albums.sel-1, songs.sel-1, &pl);
                    play_song(songs.id[songs.sel-1], fp, &pl, &conn);
                }
                if (c == ' ')
                {
                    add_song(info, artists.sel-1, albums.sel-1, songs.sel-1, &pl);
                }
            }
            if (c == '2')
            {
                current_panel = 2;
            }
        }

        /* Playlist panel */
        if (current_panel == 2)
        {
            for (int i=0; i < h_windows + v_windows; i++)
            {
                werase(windows[i]);
                wrefresh(windows[i]);
            }
            print_window(windows_pl, 1, 1, NULL, info_pl, appearance);
            if (c == KEY_DOWN && pl.sel < pl.len)
            {
                pl.sel += 1;
            }
            if (c == KEY_UP && pl.sel > 1)
            { 
                pl.sel -= 1;
            }
            if (c == '1')
            {
                current_panel = 1;
            }
            if (c == 10 || c == KEY_ENTER)
            {
                play_song(pl.songs[pl.sel-1].id, fp, &pl, &conn);
            }
            if (c == 'd')
            {
                if (pl.playing == 0)
                {
                    delete_song(pl.songs[pl.sel-1].id, &pl, info_pl);
                }
                else if (pl.sel == pl.len)
                {
                    control_player("stop", &pl, &conn);
                    delete_song(pl.songs[pl.sel-1].id, &pl, info_pl);
                }
                else if (pl.sel-1 == pl.current_playing)
                {
                    control_player("stop", &pl, &conn);
                    delete_song(pl.songs[pl.sel-1].id, &pl, info_pl);
                    play_song(pl.songs[pl.sel-1].id, fp, &pl, &conn);
                }
                else
                {
                    delete_song(pl.songs[pl.sel-1].id, &pl, info_pl);
                }
            }
            if (c == 'c' && pl.len > 0)
            {
                if (erase_playlist(windows_pl, 1, "Clear current playlist (y/n)?") == 1)
                {
                    control_player("stop", &pl, &conn);
                    while(pl.len > 0)
                    {
                        delete_song(pl.songs[0].id, &pl, info_pl);
                    }
                }
            }
        }
    
        /* Generic keybindings, operative anywhere */
        if (c=='q')
        {
            endwin();
            for (int i=0; i < h_windows + v_windows; i++)
            {
                delwin(windows[i]);
            }
            for (int i=0; i < p_windows; i++)
            {
                delwin(windows_pl[i]);
            }
            free_struct_info(&artists);
            free_struct_info(&albums);
            free_struct_info(&songs);
            free_struct_pl(&pl);
            exit(0);
        }
        if (c == 'p' && pl.playing > 0)
        {
            control_player("toggle", &pl, &conn);
        }
        if (c == 's' && pl.playing > 0)
        {
            control_player("stop", &pl, &conn);
        }
        if (c == '>')
        {
            control_player("next", &pl, &conn);
        }
        if (c == '<')
        {
            control_player("prev", &pl, &conn);
        }
    }
    return 0;
}
